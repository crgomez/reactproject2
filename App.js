/* import { Body, Container, Content, Header, Title } from 'native-base'; */
import React, { Component, useState } from 'react';
import { Text, View } from 'react-native';
/* import WebView from 'react-native-webview'; */

import Expo from 'expo';

import * as Font from 'expo-font';
import { AppLoading } from 'expo-app-loading'

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
}

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      isReady: false
    }
  }

  /* state = {
    uri: 'https://drive.google.com/viewerng/viewer?embedded=false&url=http://bibliotecadigital.ilce.edu.mx/Colecciones/ObrasClasicas/_docs/10Cuentos_LasMilNoches.pdf'
  } */

  async componentDidMount() {
    await Expo.Font.loadAsync({
      'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
      'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
    });
    this.setState({ isReady: true });
    /* const [fontLoaded, setFontLoaded] = useState(false);

    if (!fontLoaded) {
      return <AppLoading
        startAsync={fetchFonts}
        onFinish={() => setFontLoaded(true)}
      />
    } */


  }
  render() {
   /*  if (!this.state.isReady) {
      return <AppLoading />
    } */

    return (
      <View>
        <Text>Open up App.js to start working on yout app!</Text>
      </View>
    );
  }

}



{/* <Container>
        <Header>
          <Body>
            <Title>Este es el visor de PDF</Title>
          </Body>
        </Header>
        <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
          <WebView bounces={false} source={{ uri: this.state.uri }}></WebView>
        </Content>
      </Container> */
}

